package com.itau.fizzbuzz;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class FizzBuzz 
{
	public static void main( String[] args )
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Insira o valor:");
		String entrada = scanner.nextLine();
		System.out.println(loopFizzBuzz(Integer.parseInt(entrada)));
	}

	public static String validaNumero (int numero) {
		if(resto(3, numero) == 0 && resto(5, numero) == 0) {
			return "FizzBuzz";
		} 

		if(resto(3, numero) == 0){
			return "Fizz";
		}

		if(resto(5, numero) == 0)
		{
			return "Buzz";
		}

		return Integer.toString(numero);

	}

	public static int resto(int divisor, int numero) {
		return (numero % divisor);
	}

	public static String loopFizzBuzz(int valor) {
		String retorno = "";
		for(int i=1; i<=valor; i++) {
			if(i!=1)
				retorno += " - "+validaNumero(i);
			else
				retorno += validaNumero(i);
		}
		return retorno;
	}



}
