package com.itau.fizzbuzz;

import static org.junit.Assert.assertNotEquals;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class FizzBuzzTest extends TestCase
{
	
	public FizzBuzzTest( String testName )
	{
		super( testName );
	}

	public static Test suite()
	{
		return new TestSuite( FizzBuzzTest.class );
	}
	
	public void testFizzBuzzMainZero()
	{
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.loopFizzBuzz(0);
		assertEquals("", retorno);
	}
	
	public void testFizzBuzzMain()
	{
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.loopFizzBuzz(4);
		assertEquals("1 - 2 - Fizz - 4", retorno);
	}
	
	public void testFizzBuzzMainCornCase()
	{
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.loopFizzBuzz(2);
		assertNotEquals("- 1 - 2", retorno);
	}
	
	public void testFizzBuzzValidaNumero5()
	{
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = FizzBuzz.validaNumero(5);
		assertEquals("Buzz", retorno);	

	}
	
	public void testFizzBuzzValidaNumero3()
	{
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = FizzBuzz.validaNumero(3);
		assertEquals("Fizz", retorno);	

	}
	
	public void testFizzBuzzValidaNumero15()
	{
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = FizzBuzz.validaNumero(15);
		assertEquals("FizzBuzz", retorno);
	}
	
	public void testFizzBuzzValidaNumeroDiferente()
	{
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = FizzBuzz.validaNumero(2);
		assertEquals("2", retorno);
	}
	
	public void testFizzBuzzValidaResto5()
	{
		FizzBuzz fizzBuzz = new FizzBuzz();
		int retorno = FizzBuzz.resto(3, 15);
		assertEquals(0, retorno);
	}
	
	public void testFizzBuzzValidaResto3()
	{
		FizzBuzz fizzBuzz = new FizzBuzz();
		int retorno = FizzBuzz.resto(5, 10);
		assertEquals(0, retorno);
	}
	
	
}
